package kvpaxos;
import paxos.Paxos;
import paxos.State;
// You are allowed to call Paxos.Status to check if agreement was made.

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.locks.ReentrantLock;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

// TODO: remove all print statements from this file

public class Server implements KVPaxosRMI {

    ReentrantLock mutex;
    Registry registry;
    Paxos px;
    int me;

    String[] servers;
    int[] ports;
    KVPaxosRMI stub;

    // Your definitions here
    Map<String, Integer> kvMap;
    int currentSeq;

    public Server(String[] servers, int[] ports, int me){
        this.me = me;
        this.servers = servers;
        this.ports = ports;
        this.mutex = new ReentrantLock();
        this.px = new Paxos(me, servers, ports);
        // Your initialization code here

        this.kvMap = new HashMap<String, Integer>();
        this.currentSeq = 0;


        try{
            System.setProperty("java.rmi.server.hostname", this.servers[this.me]);
            registry = LocateRegistry.getRegistry(this.ports[this.me]);
            stub = (KVPaxosRMI) UnicastRemoteObject.exportObject(this, this.ports[this.me]);
            registry.rebind("KVPaxos", stub);
        } catch(Exception e){
            e.printStackTrace();
        }
    }


    // RMI handlers
    public Response Get(Request req){
        // Your code here
        Integer seq = this.px.Max() + 1;
        Op newOp = new Op("Get", seq, req.key, req.value);
        this.px.Start(seq, newOp);

        Response response = new Response(false, -1);

        int to = 10;
        for(int i = 0; i < 30; i++){
            // If a new operation is decided, reset the timers and then commit the value
            if (this.px.Status(this.currentSeq).state == State.Decided){
                i = 0;
                to = 10;
                // Retrieve decided object and attempt to cast to Op
                Object decidedObject = this.px.Status(this.currentSeq).v;
                if (Op.class.isInstance(decidedObject)) {
                    Op decidedOp = Op.class.cast(decidedObject);
                    if (decidedOp.op.equals("Put")) {
                        this.kvMap.put(decidedOp.key, decidedOp.value);
                    }
                    else if (decidedOp.op.equals("Get")) {
                        // If this is my get request and it is successful, I will respond with success
                        if (this.currentSeq == seq && decidedOp.key.equals(req.key)) {
                            response.success = true;
                            response.value = this.kvMap.get(req.key);
                        }
                    }
                }
                // If the object is not type Op, what do we do?
                else {
                    System.out.println("Bad Operation.");
                }
                // We are done with this instance, so we signal to the other peers and then increment our sequence number
                this.px.Done(this.currentSeq);
                this.currentSeq++;
            }

            // If we have committed all values up to our sequence number
            if (this.currentSeq > seq) {
                break;
            }
            // Timer stuff
            try {
                Thread.sleep(to);
            } catch (Exception e){
                e.printStackTrace();
            }
            if(to < 1000){
                to = to * 2;
            }
        }
        // PrintMap();
        return response;
    }

    public Response Put(Request req){
        // Your code here
        Integer seq = this.px.Max() + 1;
        Op newOp = new Op("Put", seq, req.key, req.value);
        this.px.Start(seq, newOp);

        Response response = new Response(false, -1);

        int to = 10;
        for(int i = 0; i < 30; i++){
            // If a new operation is decided, reset the timers and then commit the value
            if (this.px.Status(this.currentSeq).state == State.Decided){
                i = 0;
                to = 10;
                // Retrieve decided object and attempt to cast to Op
                Object decidedObject = this.px.Status(this.currentSeq).v;
                if (Op.class.isInstance(decidedObject)) {
                    Op decidedOp = Op.class.cast(decidedObject);
                    if (decidedOp.op.equals("Put")) {
                        // If this is my put request and it is successful, I will respond with success
                        if (this.currentSeq == seq && decidedOp.key.equals(req.key) && decidedOp.value.equals(req.value)) {
                            response.success = true;
                        }
                        // Commit the decided value regardless
                        this.kvMap.put(decidedOp.key, decidedOp.value);
                    }
                }
                // If the object is not type Op, need to figure out what to do
                else {
                    System.out.println("Bad operation.");
                }
                // We are done with this instance, so we signal to the other peers and then increment our sequence number
                this.px.Done(this.currentSeq);
                this.currentSeq++;
            }

            // If we have committed all values up to our sequence number
            if (this.currentSeq > seq) {
                break;
            }
            // Timer stuff
            try {
                Thread.sleep(to);
            } catch (Exception e){
                e.printStackTrace();
            }
            if(to < 1000){
                to = to * 2;
            }
        }
        // PrintMap();
        return response;
    }

    public void PrintMap() {
        for(Iterator<Map.Entry<String, Integer>> it = this.kvMap.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, Integer> entry = it.next();
		}
    }
}
