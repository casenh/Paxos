package paxos;
import java.io.Serializable;

/**
 * Please fill in the data structure you use to represent the response message for each RMI call.
 * Hint: You may need a boolean variable to indicate ack of acceptors and also you may need proposal number and value.
 * Hint: Make it more generic such that you can use it for each RMI call.
 */
public class Response implements Serializable {
    static final long serialVersionUID=2L;

	// Indicate whether the request (either a "Prepare" or "Accept" request)
	// was successful.
	boolean prepareOk;
	boolean acceptOk;
	boolean alreadyDecided;

	// Include information about the most recent acceptance values that our
	// instance has made.
	int n_a;
	Object v_a;

	/**
	 * Default constructor that replies with a "null" response.
	 */
	public Response() {
		this.prepareOk = false;
		this.acceptOk = false;
		this.alreadyDecided = false;
		this.n_a = -1;
		this.v_a = -1;
	}

	/**
	 * Prepare an actual response, indicating if the request (eg prepare,
	 * accept) was accepted and update the most recent values.
	 */
	public Response(boolean prepareOk, boolean acceptOk, boolean alreadyDecided, int n_a, Object v_a) {
		this.prepareOk = prepareOk;
		this.acceptOk = acceptOk;
		this.alreadyDecided = alreadyDecided;
		this.n_a = n_a;
		this.v_a = v_a;
	}

}
