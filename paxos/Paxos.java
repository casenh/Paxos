package paxos;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class is the main class you need to implement paxos instances.
 */

 // TODO: remove all print statements from this file
public class Paxos implements PaxosRMI, Runnable{

    ReentrantLock mutex;
    String[] peers; // hostname
    int[] ports; // host port
    int me; // index into peers[]

    Registry registry;
    PaxosRMI stub;

    AtomicBoolean dead;// for testing
    AtomicBoolean unreliable;// for testing

    // Your data here
	Map<Integer, Instance> instanceMap = new HashMap<Integer, Instance>();	

	int seq;
	Object value;
	int maxSeq;

	// Track information about the largest sequence number all other instances
	// have encountered so far. Each entry in the array corresponds to the peer
	// instance number (ie. this.me).
	Integer[] doneList;


    /**
     * Call the constructor to create a Paxos peer.
     * The hostnames of all the Paxos peers (including this one)
     * are in peers[]. The ports are in ports[].
     */
    public Paxos(int me, String[] peers, int[] ports){

        this.me = me;
        this.peers = peers;
        this.ports = ports;
        this.mutex = new ReentrantLock();
        this.dead = new AtomicBoolean(false);
		this.unreliable = new AtomicBoolean(false);

		this.doneList = new Integer[peers.length];
		for (int i=0; i < this.doneList.length; i++)
            doneList[i] = -1;
            
        this.seq = -1;
        this.value = "default";
        this.maxSeq = -1;

        // register peers, do not modify this part
        try{
            System.setProperty("java.rmi.server.hostname", this.peers[this.me]);
            registry = LocateRegistry.createRegistry(this.ports[this.me]);
            stub = (PaxosRMI) UnicastRemoteObject.exportObject(this, this.ports[this.me]);
            registry.rebind("Paxos", stub);
        } catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Call() sends an RMI to the RMI handler on server with
     * arguments rmi name, request message, and server id. It
     * waits for the reply and return a response message if
     * the server responded, and return null if Call() was not
     * be able to contact the server.
     *
     * You should assume that Call() will time out and return
     * null after a while if it doesn't get a reply from the server.
     *
     * Please use Call() to send all RMIs and please don't change
     * this function.
     */
    public Response Call(String rmi, Request req, int id){
        Response callReply = null;

        PaxosRMI stub;
        try{
            Registry registry=LocateRegistry.getRegistry(this.ports[id]);
            stub=(PaxosRMI) registry.lookup("Paxos");
            if(rmi.equals("Prepare"))
                callReply = stub.Prepare(req);
            else if(rmi.equals("Accept"))
                callReply = stub.Accept(req);
            else if(rmi.equals("Decide"))
                callReply = stub.Decide(req);
            else
                System.out.println("Wrong parameters!");
        } catch(Exception e){
            return null;
        }
        return callReply;
    }


    /**
     * The application wants Paxos to start agreement on instance seq,
     * with proposed value v. Start() should start a new thread to run
     * Paxos on instance seq. Multiple instances can be run concurrently.
     *
     * Hint: You may start a thread using the runnable interface of
     * Paxos object. One Paxos object may have multiple instances, each
     * instance corresponds to one proposed value/command. Java does not
     * support passing arguments to a thread, so you may reset seq and v
     * in Paxos object before starting a new thread. There is one issue
     * that variable may change before the new thread actually reads it.
     * Test won't fail in this case.
     *
     * Start() just starts a new thread to initialize the agreement.
     * The application will call Status() to find out if/when agreement
     * is reached.
     */
    public void Start(int seq, Object value){

		// Launch the Paxos proposal run in a new thread so we don't block on
		// the response. This will allow mulitple runs to be started
		// concurrently.
        Instance instance = getInstance(seq, value);

		return;
    }

    @Override
    public void run(){

		// Used to determine how many peers have accepted a prepare or accept proposal
		int acceptCount;

		Request req;
        Response response;
        
        Instance instance = getInstance(this.seq, this.value);

		// While not decided do
		while(instance.state != State.Decided) {
			// TODO: add dead check

			acceptCount = 0;

			// Choose n, unique and higher than any n seen so far
			instance.prop = instance.n_p + 1;
			while(instance.prop % (this.peers.length) != this.me) {
				instance.prop++;
			}

			// Send prepare(n) to all servers including self
			req = new Request(instance.seq, instance.prop, instance.value, this.doneList);
			for (int id=0; id < this.peers.length; id++) {
                response = this.Call("Prepare", req, id);
                
                if (response == null)
                    continue;
                
                if (response.alreadyDecided) {
                    req = new Request(instance.seq, response.n_a, response.v_a);
                    Decide(req);
                }
                
				if (response.prepareOk == true)
					acceptCount += 1;
				
				// Check the >
				if (response.n_a > instance.n_a){
					instance.n_a = response.n_a;
					instance.v_a = response.v_a;
                }

				// Slight performance optimization
				// if (acceptCount > this.peers.length / 2)
				// 	break;
			}

			// If prepare_ok from majority then
			if (acceptCount > this.peers.length / 2) {
				// Reset counter
				acceptCount = 0;
				
				// v' = v_a with highest n_a; choose own v otherwise
				if (instance.n_a != -1)
					instance.value = instance.v_a;
				
				// Send accept(n, v') to all
				req = new Request(instance.seq, instance.prop, instance.value);
				for (int id=0; id < this.peers.length; id++) {
                    if (id == this.me)
                        response = Accept(req);
                    else
                        response = this.Call("Accept", req, id);
                    
                    if (response == null)
                        continue;
					if (response.acceptOk == true)
						acceptCount += 1;

					// Slight performance optimization
					// if (acceptCount > this.peers.length / 2)
					// 	break;
                }

				// if accept_ok(n) from majority then
				if (acceptCount > this.peers.length / 2) {
                    // send decided(v') to all
					req = new Request(instance.seq, instance.prop, instance.value);
					for (int id=0; id < this.peers.length; id++) {
						response = this.Call("Decide", req, id);
					}
				}
			}
		}

		return;
    }

    public Instance getInstance(int seq, Object value) {
        // Get instance object
		Thread t;
		Instance instance = this.instanceMap.get(seq);
		// In case this is a new prepare
		if (instance == null) {
            // Set the proposal state variables that should be used by this proposer.
            this.seq = seq;
            this.value = value;
            if (this.seq > this.maxSeq)
                this.maxSeq = this.seq;

            t = new Thread(this);
            instance = new Instance(seq, value, t);
            this.instanceMap.put(seq, instance);
			t.start();
        }
        return instance;
    }

    // RMI handler
    public synchronized Response Prepare(Request req){

        Instance instance = getInstance(req.seq, req.value);
		// Instance instance = getInstance(req.seq, req.value + ":" + this.me);

		if (req.doneList != null)
			this.UpdateDone(req.doneList);

		// Construct the prepare failure response by default. If this is the
		// highest prepare number we've seen, update our tracking state
		// information and notify the proposer of the successful prepare.
        Response res = new Response(false, false, false, instance.n_a, instance.v_a);
        instance.n_pLock.lock();
		if (req.prop > instance.n_p) {
			instance.n_p = req.prop;
            res.prepareOk = true;
        }
        instance.n_pLock.unlock();

        if (instance.state == State.Decided)
            res.alreadyDecided = true;

		// // TODO: Is this required? Or is this handled in the run section?
		// // If we have already accepted a value ahead of our sequence number, it
		// // is safe for us to return the decided value about the sequence
		// // number. Otherwise, it may still be being decided upon.
		// if (this.n_a >= req.seq) {
		// 	this.stateMap.put(req.seq, State.Decided);
		// }
		return res;
    }

    public synchronized Response Accept(Request req){
		
        // Get instance object
        Instance instance = getInstance(req.seq, req.value);

		// Construct the accept failure response by default. If this is the
		// highest prepare number we've seen, update our tracking state
		// information and notify the proposer of the successful accept.
        Response res = new Response(false, false, false, instance.n_a, instance.v_a);
        instance.n_pLock.lock();
        instance.n_aLock.lock();
        instance.v_aLock.lock();
		if (req.prop >= instance.n_p) {
			instance.n_p = req.prop;
			instance.n_a = req.prop;
			instance.v_a = req.value;
			res.acceptOk = true;
        }
        instance.n_pLock.unlock();
        instance.n_aLock.unlock();
        instance.v_aLock.unlock();
		return res;
    }

    public Response Decide(Request req){
		Instance instance = getInstance(req.seq, req.value);

		// Update our most recent acceptance values.
		instance.n_a = req.seq;
		instance.v_a = req.value;
		instance.value = req.value;

		instance.state = State.Decided;
		Response res = new Response(true, true, true, 0, null);
		return res;
    }

    /**
     * The application on this machine is done with
     * all instances <= seq.
     *
     * see the comments for Min() for more explanation.
     */
    // TODO: test case for Done
    public void Done(int seq) {
		this.doneList[this.me] = seq;
		for(Iterator<Map.Entry<Integer, Instance>> it = this.instanceMap.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<Integer, Instance> entry = it.next();
			if(entry.getKey() <= seq) {
				it.remove();
			}
		}
    }


	/**
	 * Update our local information for what "done" value for each instance is.
	 */
	private void UpdateDone(Integer[] doneList) {
		for (int i=0; i < doneList.length; i++) {
			if (doneList[i] > this.doneList[i]) {
				this.doneList[i] = doneList[i];
			}
		}
	}


    /**
     * The application wants to know the
     * highest instance sequence known to
     * this peer.
     */
    public int Max(){
		return this.maxSeq;
    }

    /**
     * Min() should return one more than the minimum among z_i,
     * where z_i is the highest number ever passed
     * to Done() on peer i. A peers z_i is -1 if it has
     * never called Done().

     * Paxos is required to have forgotten all information
     * about any instances it knows that are < Min().
     * The point is to free up memory in long-running
     * Paxos-based servers.

     * Paxos peers need to exchange their highest Done()
     * arguments in order to implement Min(). These
     * exchanges can be piggybacked on ordinary Paxos
     * agreement protocol messages, so it is OK if one
     * peers Min does not reflect another Peers Done()
     * until after the next instance is agreed to.

     * The fact that Min() is defined as a minimum over
     * all Paxos peers means that Min() cannot increase until
     * all peers have been heard from. So if a peer is dead
     * or unreachable, other peers Min()s will not increase
     * even if all reachable peers call Done. The reason for
     * this is that when the unreachable peer comes back to
     * life, it will need to catch up on instances that it
     * missed -- the other peers therefore cannot forget these
     * instances.
     */
    public int Min(){
		return Collections.min(Arrays.asList(this.doneList)) + 1;
    }



    /**
     * the application wants to know whether this
     * peer thinks an instance has been decided,
     * and if so what the agreed value is. Status()
     * should just inspect the local peer state;
     * it should not contact other Paxos peers.
     */
    public retStatus Status(int seq){
		Instance instance = this.instanceMap.get(seq);
		retStatus ret;
		if (instance == null) {
			ret = new retStatus(State.Pending, null);
		}
		else {
			ret = new retStatus(instance.state, instance.value);
		}
		
		return ret;
    }

    /**
     * helper class for Status() return
     */
    public class retStatus{
        public State state;
        public Object v;

        public retStatus(State state, Object v){
            this.state = state;
            this.v = v;
        }
    }

    /**
     * Tell the peer to shut itself down.
     * For testing.
     * Please don't change these four functions.
     */
    public void Kill(){
        this.dead.getAndSet(true);
        if(this.registry != null){
            try {
                UnicastRemoteObject.unexportObject(this.registry, true);
            } catch(Exception e){
                System.out.println("None reference");
            }
        }
    }

    public boolean isDead(){
        return this.dead.get();
    }

    public void setUnreliable(){
        this.unreliable.getAndSet(true);
    }

    public boolean isunreliable(){
        return this.unreliable.get();
    }


}
