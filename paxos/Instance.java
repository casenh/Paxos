package paxos;

import java.util.concurrent.locks.ReentrantLock;

public class Instance {

    // State of the instance
    State state;

	/* Proposer state variables. */
	int seq;		// The sequence number of the instance.
	int prop;		// The current proposal number.
	Object value;	// The value being proposed.

	/* Acceptor state variables. */
	int n_p;	// Highest prepare number seen.
	int n_a;	// Highest accept number seen.
    Object v_a; // Value of the highest accept seen.

    // Locks for shared variables
    ReentrantLock n_pLock;
    ReentrantLock n_aLock;
    ReentrantLock v_aLock;

    // Thread for this instance
    Thread t; 
    
    public Instance(int seq, Object value, Thread t) {
        // Initialize default variables
        this.state = State.Pending;
		this.seq = seq;
		this.value = value;
		this.n_p = -1;
		this.n_a = -1;
        this.v_a = null;
        this.t = t;

        n_pLock = new ReentrantLock();
        n_aLock = new ReentrantLock();
        v_aLock = new ReentrantLock();
    }
}